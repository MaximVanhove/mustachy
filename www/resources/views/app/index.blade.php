<!DOCTYPE html>
<html lang="nl" ng-app="app">
    <head>
        <meta charset="utf-8">
        <title>Mustachy</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/favicon/apple-touch-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/favicon/apple-touch-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/favicon/apple-touch-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/favicon/apple-touch-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/favicon/apple-touch-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/favicon/apple-touch-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/favicon/apple-touch-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/favicon/apple-touch-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/favicon/apple-touch-icon-180x180.png') }}">
        <link rel="icon" type="image/png" href="{{ asset('/favicon/favicon-32x32.png') }}" sizes="32x32">
        <link rel="icon" type="image/png" href="{{ asset('/favicon/favicon-194x194.png') }}" sizes="194x194">
        <link rel="icon" type="image/png" href="{{ asset('/favicon/favicon-96x96.png') }}" sizes="96x96">
        <link rel="icon" type="image/png" href="{{ asset('/favicon/android-chrome-192x192.png') }}" sizes="192x192">
        <link rel="icon" type="image/png" href="{{ asset('/favicon/favicon-16x16.png') }}" sizes="16x16">
        <link rel="manifest" href="{{ asset('/favicon/manifest.json') }}">
        <link rel="mask-icon" href="{{ asset('/favicon/safari-pinned-tab.svg') }}" color="#70b9a9">
        <link rel="shortcut icon" href="{{ asset('/favicon/favicon.ico') }}">
        <meta name="msapplication-TileColor" content="#70b9a9">
        <meta name="msapplication-TileImage" content="{{ asset('/favicon/mstile-144x144.png') }}">
        <meta name="msapplication-config" content="{{ asset('/favicon/browserconfig.xml') }}">
        <meta name="theme-color" content="#70b9a9">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ url('/app/css/main.css') }}">

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,200,100,500,700,600' rel='stylesheet' type='text/css'>

        <!-- Scripts -->
        <script src="{{ url('/app/vendor/angular/angular.js') }}"></script>
        <script src="{{ url('/app/vendor/faker/faker.min.js') }}"></script>
        <script src="{{ url('/app/vendor/lodash/lodash.min.js') }}"></script>
        <script src="{{ url('/app/js/app.js') }}"></script>
        <script>
          angular.module("app").value('URL', '{{ url('/') }}');
        </script>
    </head>
    <body ng-controller="AppController as app">
        <div ui-view></div>
    </body>
</html>

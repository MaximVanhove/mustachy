@extends('layouts.backoffice', [ 'activeTab' => 'drinks' ])

@section('content')
<div class="container" style="padding: 20px">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Categorie</div>

                <div class="panel-body">
        			{!! Form::open(array('url' => 'categories', 'files' => true)) !!}
                    @if(count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach( $errors->all() as $error )
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
            			{!! Form::label('name', 'Naam:') !!}
            			{!! Form::text('name',null,['class'=>'form-control']) !!}
            		</div>
                    <div class="form-group">
                        <div>
                            {!! Form::label('image', 'Achtergrond afbeelding:') !!}
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-5">
                                <div class="well logo">
                                    <img class="img-responsive" id="src-image" src="">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-7">
                                <a class="btn btn-warning" id="update-image" href>Kies een afbeelding</a>
                                <div class="hidden">
                                    {!! Form::file('image', ['id' => 'upload-image']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
        		    <div class="form-group">
        		        {!! Form::submit('Maak een nieuwe categorie', ['class' => 'btn btn-primary form-control']) !!}
        		    </div>
        		    {!! Form::close() !!}
                </div>
            </div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$().ready(function(){
    // trigger click on hidden file input
    $('#update-image').on('click', function(){ $('#upload-image').click(); });

    // load choosen image
    $('#upload-image').change(function(){ srcImage(this, '#src-image'); });

    function srcImage(input, selector) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(selector).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
});
</script>
@endsection

@extends('layouts.backoffice', [ 'activeTab' => 'drinks', 'body' => ' categories ' ])

@section('content')

<div id="category-wrapper">
    <section id="categories">
        <section id="category-tabs">
            <ul class="c-horizontal-tabs">
                @foreach ($categories as $item)
                <li class="c-horizontal-tab {{ $item->id == $category->id ? 'is-active' : '' }}">
                    <a href="{{ route('categories.show', ['id' => $item->id]) }}" data-id="$item->id">{{ $item->name }}</a>
                </li>
                @endforeach
            </ul>
        </section>
        <section id="category-content">

                <div class="row">
                    <div class="col-sm-12">
                        {!! Form::open([
                            'method' => 'GET',
                            'route' => ['categories.create']
                        ]) !!}
                        <button class="button icon">
                            Voeg een nieuwe categorie toe
                            <i>
                                <svg preserveAspectRatio="xMidYMid meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 64 48.311" enable-background="new 0 0 64 48.311" xml:space="preserve">
                                <g>
                                  <polygon fill="#FFFFFF" points="24.474,48.311 0,23.844 8.798,15.042 24.474,30.711 55.181,0 63.98,8.8 	"/>
                                </g>
                                </svg>
                            </i>
                        </button>
                        {!! Form::close() !!}
                    </div>
                </div>

                <h2>{{ $category->name }}</h2>

                <div class="row top-buffer">
                    <div class="col-sm-6 col-sm-push-6">
                        <h3>Instellingen</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                {!! Form::model($category, [
            						'method' => 'PATCH',
            						'route' => ['categories.update', $category->id],
                                    'files' => true
            					]) !!}
                                <div class="form-group">
                                    @if(isset($success))
                                    <div class="alert alert-success"> {{ $success }} </div>
                                    @endif
                                    <div class="alert-warning">
                                        @foreach( $errors->all() as $error )
                                           <br> {{ $error }}
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group">
                        			{!! Form::label('name', 'Naam:') !!}
                        			{!! Form::text('name',null,['class'=>'form-control']) !!}
                        		</div>
                                <div class="form-group">
                                    <div>
                                        {!! Form::label('image', 'Achtergrond afbeelding:') !!}
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-lg-5">
                                            <div class="well logo">
                                                <img class="img-responsive" id="src-image" src="{{ $category->image }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-7">
                                            <a class="btn btn-warning" id="update-image" href>Verander afbeelding</a>
                                            <div class="hidden">
                                                {!! Form::file('image', ['id' => 'upload-image']) !!}
                                            </div>
                                        </div>
                                    </div>
                    		    </div>
                    		    <div class="form-group">
                    		        {!! Form::submit('Opslaan', ['class' => 'btn btn-primary form-control']) !!}
                    		    </div>
                    		    {!! Form::close() !!}
                            </div>
                		</div>
                        <div class="row top-buffer">
                            <div class="col-sm-12">
                                {!! Form::open([
                                    'method' => 'DELETE',
                                    'route' => ['categories.destroy', $category->id]
                                ]) !!}
                                        <button type="submit" class="btn btn-danger">Verwijder deze categorie &nbsp;&nbsp;<i class="fa fa-trash" aria-hidden="true"></i></button>
                                {!! Form::close() !!}
                            </div>
                		</div>
                    </div>
                    <div class="col-sm-6 col-sm-pull-6">
                        <h3>Producten</h3>
                        <div class="row top-buffer">
                            <div class="col-sm-12">
                                <a class="btn btn-default btn-sm" href="{{ url('/drinks/create/'.$category->id) }}">Voeg een product toe</a>
                            </div>
                		</div>
                        <div class="row top-buffer">
                            <div class="col-sm-12">
                                @if ($drinks == null || count($drinks) < 1)
                                    <p></p>
                                    <p>
                                        Deze categorie heeft nog geen producten
                                    </p>
                                @else
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <td><b>Naam</b></td>
                                            <td><b>Prijs</b></td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($drinks as $drink)
                                        <tr>
                                            <td>{{ $drink->name }}</td>
                                            @if ($drink->promoted)
                                            <td><strike>{{ $drink->price }}</strike> {{ $drink->promoted_price }}</td>
                                            @else
                                            <td>{{ $drink->price }}</td>
                                            @endif
                                            <td align="right">
                                                {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'route' => ['drinks.destroy', $drink->id]
                                                ]) !!}
                                                    <a class="btn btn-sm btn-primary" href="{{ URL::to('drinks/' . $drink->id . '/edit')}}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <button type="submit" class="btn btn-danger  btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                		</div>
                    </div>
                </div>

                <div class="row top-buffer">
                    <div class="col-sm-12">

                    </div>
            	</div>
        </section>
    </section>
</div>
@endsection

@section('scripts')
<script>
$().ready(function(){
    // trigger click on hidden file input
    $('#update-image').on('click', function(){ $('#upload-image').click(); });

    // load choosen image
    $('#upload-image').change(function(){ srcImage(this, '#src-image'); });

    function srcImage(input, selector) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(selector).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
});
</script>
@endsection

@extends('layouts.backoffice', [ 'activeTab' => 'cart' ])

@section('content')

<section id="orders" class="version-2" ng-controller="OrderController as vm">
    <h1 class="c-order-group-title">Nieuwe bestellingen</h1>

    <section class="c-orders-wrapper">
        <section class="c-orders-container">
            <article class="c-order new" ng-repeat="order in vm.orders | filter: { status: 0 }">
                <table class="c-order-products">
                    <tbody>
                        <tr class="c-order-product" ng-repeat="item in order.ordered_drinks">
                            <td class="c-order-product-name">
                                🌜 item.drink.name 🌛
                            </td>
                            <td class="c-order-product-amount">
                                🌜 item.amount 🌛
                            </td>
                        </tr>
                        <tr class="c-order-total">
                            <td class="c-order-total-price">
                                € 🌜order.tobepayed🌛
                            </td>
                            <td class="c-order-total-amount">
                                🌜order.totaldrinks🌛
                            </td>
                        </tr>
                    </tbody>
                </table>
                <section class="c-order-info">
                    <p class="c-order-info-uuid">#🌜 order.uuid 🌛</p>
                    <p class="c-order-info-date">🌜 order.created_at 🌛</p>
                </section>
                <section class="c-order-note" ng-hide="order.note == ''">
                    <p class="c-order-note-title">Opmerking:</p>
                    <p class="c-order-info-content">🌜 order.note 🌛</p>
                </section>
                <section class="c-order-actions">
                    <button ng-click="vm.move(order)" class="button full icon">
                        afwerken
                        <i>
                            <svg preserveAspectRatio="xMidYMid meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 64 48.311" enable-background="new 0 0 64 48.311" xml:space="preserve">
                            <g>
                              <polygon fill="#FFFFFF" points="24.474,48.311 0,23.844 8.798,15.042 24.474,30.711 55.181,0 63.98,8.8 	"/>
                            </g>
                            </svg>
                        </i>
                    </button>
                </section>
            </article>
        </section>
    </section>

    <h1 class="c-order-group-title c-green">Afgewerkte bestellingen</h1>

    <section class="c-orders-wrapper">
        <section class="c-orders-container">
            <article class="c-order finished" ng-repeat="order in vm.orders | filter: { status: 1 }">
                <table class="c-order-products">
                    <tbody>
                        <tr class="c-order-product" ng-repeat="item in order.ordered_drinks">
                            <td class="c-order-product-name">
                                🌜 item.drink.name 🌛
                            </td>
                            <td class="c-order-product-amount">
                                🌜 item.amount 🌛
                            </td>
                            <tr class="c-order-total">
                                <td class="c-order-total-price">
                                    € 🌜order.tobepayed🌛
                                </td>
                                <td class="c-order-total-amount">
                                    🌜order.totaldrinks🌛
                                </td>
                            </tr>
                        </tr>
                    </tbody>
                </table>
                <section class="c-order-info">
                    <p class="c-order-info-uuid">#🌜 order.uuid 🌛</p>
                    <p class="c-order-info-date">🌜 order.created_at 🌛</p>
                </section>
                <section class="c-order-note">
                    <p class="c-order-note-title">Opmerking:</p>
                    <p class="c-order-info-content">Ice tea zonder ijs</p>
                </section>
                <section class="c-order-actions">
                    <button ng-click="vm.move(order)" class="button full icon blue">
                        betaald
                        <i>
                            <svg preserveAspectRatio="xMidYMid meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 64 48.311" enable-background="new 0 0 64 48.311" xml:space="preserve">
                            <g>
                              <polygon fill="#FFFFFF" points="24.474,48.311 0,23.844 8.798,15.042 24.474,30.711 55.181,0 63.98,8.8 	"/>
                            </g>
                            </svg>
                        </i>
                    </button>
                </section>
            </article>
        </section>
    </section>

    <h1 class="c-order-group-title c-blue">Betaalde bestellingen</h1>

    <section class="c-orders-wrapper">
        <section class="c-orders-container">
            <article class="c-order payed" ng-repeat="order in vm.orders | filter: { status: 2 }">
                <table class="c-order-products">
                    <tbody>
                        <tr class="c-order-product" ng-repeat="item in order.ordered_drinks">
                            <td class="c-order-product-name">
                                🌜 item.drink.name 🌛
                            </td>
                            <td class="c-order-product-amount">
                                🌜 item.amount 🌛
                            </td>
                            <tr class="c-order-total">
                                <td class="c-order-total-price">
                                    € 🌜order.tobepayed🌛
                                </td>
                                <td class="c-order-total-amount">
                                    🌜order.totaldrinks🌛
                                </td>
                            </tr>
                        </tr>
                    </tbody>
                </table>
                <section class="c-order-info">
                    <p class="c-order-info-uuid">#🌜 order.uuid 🌛</p>
                    <p class="c-order-info-date">🌜 order.created_at 🌛</p>
                </section>
                <section class="c-order-note">
                    <p class="c-order-note-title">Opmerking:</p>
                    <p class="c-order-info-content">Ice tea zonder ijs</p>
                </section>
                <section class="c-order-actions">
                    <button ng-click="vm.move(order)" class="button full red">prullenmand</button>
                </section>
            </article>
        </section>
    </section>
</section>
@endsection

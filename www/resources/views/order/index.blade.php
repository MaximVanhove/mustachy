@extends('layouts.backoffice', [ 'activeTab' => 'cart' ])

@section('content')


<section id="orders" class="version-1">
    <section class="c-icon-tabs-wrapper">
        <ul class="c-icon-tabs">
            <li class="c-icon-tab {{ $tab == '-1' ? 'is-active' : '' }}">
                <a href="{{ route('orders.index', 'all') }}"><i class="fa fa-align-left" aria-hidden="true"></i></a>
            </li>
            <li class="c-icon-tab c-red {{ $tab == '0' ? 'is-active' : '' }}">
                <a href="{{ route('orders.index', 'new') }}"><i class="fa fa-bolt" aria-hidden="true"></i></a>
            </li>
            <li class="c-icon-tab c-green {{ $tab == '1' ? 'is-active' : '' }}">
                <a href="{{ route('orders.index', 'finished') }}"><i class="fa fa-check" aria-hidden="true"></i></a>
            </li>
            <li class="c-icon-tab c-blue {{ $tab == '2' ? 'is-active' : '' }}">
                <a href="{{ route('orders.index', 'payed') }}"><i class="fa fa-eur" aria-hidden="true"></i></a>
            </li>
            <li class="c-icon-tab {{ $tab == '3' ? 'is-active' : '' }}">
                <a href="{{ route('orders.index', 'archive') }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
            </li>
        </ul>
        <section class="c-icon-tabs-content">
            <h1 class="{{ $tabname == 'all' ? 'c-grey' : '' }}{{ $tabname == 'new' ? 'c-red' : '' }}{{ $tabname == 'finished' ? 'c-green' : '' }}{{ $tabname == 'payed' ? 'c-blue' : '' }}{{ $tabname == 'archive' ? 'c-grey' : '' }}">
                {{ $tabtitle }}
            </h1>

            @foreach ($orders as $order)
            <article class="c-order">
                <header class="c-order-header {{ $order->status == '0' ? ' red' : '' }}{{ $order->status == '1' ? ' green' : '' }}{{ $order->status == '2' ? ' blue' : '' }}{{ $order->status == '3' ? ' grey' : '' }}">
                    Bestelling #{{ $order->uuid }}
                </header>
                <table class="c-order-products">
                    <tbody>
                        @foreach ($order->orderedDrinks as $purchase)
                        <tr class="c-order-product">
                            <td class="c-order-product-cell">
                                {{ $purchase->drink->name }}
                            </td>
                            <td class="c-order-product-cell">
                                € {{ $purchase->drink->current_price }}
                            </td>
                            <td class="c-order-product-cell">
                                x
                            </td>
                            <td class="c-order-product-cell">
                                <b>{{ $purchase->amount }}</b>
                            </td>
                            <td class="c-order-product-cell">
                                =
                            </td>
                            <td class="c-order-product-cell">
                                <span class="c-green">€ {{ $purchase->drink->current_price * $purchase->amount }}</span>
                            </td>
                            <td class="c-order-product-cell">
                                <a href class="button"><i class="fa fa-check" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        <tr class="c-order-product c-order-total">
                            <td class="c-order-product-cell c-order-cell-total">
                                Totaal
                            </td>
                            <td class="c-order-product-cell">
                            </td>
                            <td class="c-order-product-cell">
                            </td>
                            <td class="c-order-product-cell">
                                <b>{{ $order->totaldrinks }}</b>
                            </td>
                            <td class="c-order-product-cell">
                            </td>
                            <td class="c-order-product-cell">
                                <span class="c-green">€ {{ $order->tobepayed }}</span>
                            </td>
                            <td class="c-order-product-cell">
                            </td>
                        </tr>
                    </tbody>
                </table>

                <section class="c-order-container">
                    {{ Form::open(array(
                        'method' => 'PATCH',
                        'route' => 'orders.update'
                    )) }}
                        {{ Form::hidden('id', $order->id) }}
                        {{ Form::hidden('status', $order->status) }}
                        {{ Form::hidden('tab', $tabname) }}
                        @if ($order->status == 0)
                        <button type="submit" class="button icon">
                            Bestelling afwerken
                            <i>
                                <svg preserveAspectRatio="xMidYMid meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 64 48.311" enable-background="new 0 0 64 48.311" xml:space="preserve">
                                <g>
                                  <polygon fill="#FFFFFF" points="24.474,48.311 0,23.844 8.798,15.042 24.474,30.711 55.181,0 63.98,8.8 	"/>
                                </g>
                                </svg>
                            </i>
                        </button>
                        @endif
                        @if ($order->status == 1)
                        <button class="button icon blue">
                            Betaald
                            <i>
                                <svg preserveAspectRatio="xMidYMid meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 64 48.311" enable-background="new 0 0 64 48.311" xml:space="preserve">
                                <g>
                                  <polygon fill="#FFFFFF" points="24.474,48.311 0,23.844 8.798,15.042 24.474,30.711 55.181,0 63.98,8.8 	"/>
                                </g>
                                </svg>
                            </i>
                        </button>
                        @endif
                        @if ($order->status == 2)
                        <button class="button red">Prullenmand</button>
                        @endif
                    {{ Form::close() }}
                </section>

            </article>
            @endforeach

        </section>
    </section>
</section>
@endsection

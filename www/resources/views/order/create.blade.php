@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Categorieën</div>

                <div class="panel-body">
        			{!! Form::open(array('url' => 'categories')) !!}
                    <div class="form-group">
            			{!! Form::label('name', 'Naam:') !!}
            			{!! Form::text('name',null,['class'=>'form-control']) !!}
            		</div>
        		    <div class="form-group">
        		        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
        		    </div>
        		    {!! Form::close() !!}
                </div>
            </div>
		</div>
	</div>
</div>
@endsection

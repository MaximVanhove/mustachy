@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Deze categorie aanpassen</div>

                <div class="panel-body">
					{!! Form::model($category, [
						'method' => 'PATCH',
						'route' => ['categories.update', $category->id]
					]) !!}
                    <div class="form-group">
            			{!! Form::label('name', 'Naam:') !!}
            			{!! Form::text('name',null,['class'=>'form-control']) !!}
            		</div>
        		    <div class="form-group">
        		        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
        		    </div>
        		    {!! Form::close() !!}
                </div>
            </div>
		</div>
	</div>
</div>
@endsection

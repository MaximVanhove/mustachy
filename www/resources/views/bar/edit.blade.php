@extends('layouts.backoffice', [ 'activeTab' => 'account' ])

@section('content')
<div id="page-bar-edit" class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-1 col-md-6 col-md-offset-3">
            <!-- <h1>{{ $bar->name }}</h1> -->


            {!! Form::model($bar, [
                'method' => 'PATCH',
                'route' => ['bar.update', $bar->id],
                'files'=>true,
                'class' => 'form-horizontal'
            ]) !!}
            <div class="form-group">
                @if(isset($success))
                <div class="col-sm-9 col-sm-offset-3">
                    <div class="alert alert-success"> {{$success}} </div>
                </div>
                @endif
            </div>
            <div class="form-group">
                <div class="col-sm-3 label-control">
                    {!! Form::label('name', 'Naam:', ['class'=>'control-label']) !!}
                </div>
                <div class="col-sm-9">
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="form-group label-control">
                <div class="col-sm-3">
                    {!! Form::label('logo', 'Logo:', ['class'=>'control-label']) !!}
                </div>
                <div class="col-sm-9">
                    <div class="well logo">
                        <img class="img-responsive" src="{{$bar->logo_uri}}">
                    </div>
                    <p>
                        Je bekomt het mooiste resultaat met een transparante achtergrond
                    </p>
                    {!! Form::file('image') !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    {!! Form::submit('Opslaan', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="alert-warning">
                    @foreach( $errors->all() as $error )
                       <br> {{ $error }}
                    @endforeach
                </div>
            </div>
            {!! Form::close() !!}
		</div>
	</div>
</div>
@endsection

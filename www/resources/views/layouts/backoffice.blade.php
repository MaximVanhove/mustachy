<!DOCTYPE html>
<html lang="nl" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/favicon/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/favicon/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/favicon/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/favicon/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/favicon/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/favicon/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/favicon/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/favicon/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/favicon/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('/favicon/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('/favicon/favicon-194x194.png') }}" sizes="194x194">
    <link rel="icon" type="image/png" href="{{ asset('/favicon/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ asset('/favicon/android-chrome-192x192.png') }}" sizes="192x192">
    <link rel="icon" type="image/png" href="{{ asset('/favicon/favicon-16x16.png') }}" sizes="16x16">
    <link rel="manifest" href="{{ asset('/favicon/manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('/favicon/safari-pinned-tab.svg') }}" color="#70b9a9">
    <link rel="shortcut icon" href="{{ asset('/favicon/favicon.ico') }}">
    <meta name="msapplication-TileColor" content="#70b9a9">
    <meta name="msapplication-TileImage" content="{{ asset('/favicon/mstile-144x144.png') }}">
    <meta name="msapplication-config" content="{{ asset('/favicon/browserconfig.xml') }}">
    <meta name="theme-color" content="#70b9a9">

    <title>Mustachy | Backoffice</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,200,300,500,600,700' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables/dataTables.bootstrap.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
      angular.module("app").constant("CSRF_TOKEN", '{{ csrf_token() }}');
    </script>
</head>
<body ng-cloak>

    <div id="main-wrapper" ng-controller="AppController as app">
        <header id="header">
            <section class="logo">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="100%" height="100%" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
                    <g>
                        <circle fill="#71B9AA" cx="25" cy="24.981" r="25.018"/>
                        <g>
                            <defs>
                                <circle id="SVGID_1_" cx="25" cy="24.981" r="25.018"/>
                            </defs>
                            <clipPath id="SVGID_2_">
                                <use xlink:href="#SVGID_1_"  overflow="visible"/>
                            </clipPath>
                            <g clip-path="url(#SVGID_2_)">
                                <path fill="#E6E6E6" d="M57.505,42.148c-0.883-9.926-13.82-14.056-22.514-15.395c-2.465-0.381-9.115,6.776-10.379,7.234
                                    c-1.265-0.458-7.079-7.627-9.41-7.282C6.51,27.994-6.732,32.101-7.625,42.148h-0.049v12.854h65.229V42.148H57.505z"/>
                                <path fill="#E6E6E6" d="M35.353,23.305c-4.871,3.204-10.396,8.118-10.396,8.118s-5.525-4.914-10.396-8.118
                                    c0,0-2.008,1.411-4.188,4.743L24.956,40.61L39.54,28.048C37.361,24.716,35.353,23.305,35.353,23.305z"/>
                                <path fill="#D1D1D2" d="M48.896,50.538c-3.192-10.453-3.699-16.691-3.697-19.602c-1.653-0.697-3.375-1.267-5.071-1.729
                                    c0,0,0-0.001,0.002-0.001c-0.058-0.016-0.115-0.029-0.173-0.045c-0.217-0.059-0.436-0.114-0.65-0.169
                                    c-0.099-0.025-0.193-0.05-0.289-0.073c-0.087-0.021-0.178-0.045-0.265-0.065c0,0.001,0,0.001,0,0.001
                                    c-0.178-0.04-0.355-0.086-0.529-0.125c-3.728,2.935-13.268,11.345-13.268,11.345s-9.54-8.41-13.267-11.345
                                    c-0.176,0.039-0.354,0.085-0.529,0.127v-0.003c-0.105,0.024-0.211,0.052-0.315,0.077c-0.066,0.017-0.131,0.034-0.197,0.05
                                    c-0.251,0.063-0.503,0.129-0.755,0.197c-0.037,0.01-0.075,0.019-0.111,0.028c0.001,0,0.001,0,0.001,0.001
                                    c-1.696,0.462-3.417,1.031-5.068,1.729c0,2.91-0.506,9.148-3.701,19.602h12.229c0,0-1.143-13.583-1.883-20.049
                                    c5.119,4.231,13.597,11.735,13.597,11.735s8.477-7.504,13.597-11.735c-0.739,6.466-1.883,20.049-1.883,20.049H48.896z"/>
                                <path fill="#434854" d="M50.179,50.482c-3.364-11.015-3.897-17.586-3.897-20.653c-1.74-0.735-3.553-1.334-5.342-1.82
                                    c0,0,0,0,0.003-0.001c-0.061-0.017-0.121-0.031-0.183-0.047c-0.229-0.062-0.458-0.122-0.687-0.18
                                    c-0.101-0.026-0.201-0.05-0.302-0.075c-0.094-0.023-0.188-0.048-0.279-0.07c0,0.002-0.002,0.003-0.002,0.003
                                    c-0.186-0.045-0.373-0.09-0.557-0.133c-3.929,3.092-13.978,11.952-13.978,11.952s-10.049-8.86-13.978-11.952
                                    c-0.184,0.043-0.371,0.088-0.558,0.133l-0.001-0.003c-0.109,0.026-0.22,0.055-0.332,0.082c-0.068,0.018-0.136,0.035-0.206,0.053
                                    c-0.265,0.066-0.53,0.135-0.796,0.207c-0.039,0.011-0.077,0.02-0.116,0.03c0,0.001,0,0.001,0,0.001
                                    c-1.786,0.486-3.599,1.085-5.339,1.82c0,3.067-0.533,9.639-3.898,20.653h12.882c0,0-1.205-14.312-1.983-21.123
                                    c5.394,4.456,14.325,12.362,14.325,12.362s8.931-7.905,14.325-12.362c-0.778,6.811-1.985,21.123-1.985,21.123H50.179z"/>
                                <g>
                                    <circle fill="#BEBFC0" cx="24.956" cy="43.901" r="1.226"/>
                                    <circle fill="#ABADAE" cx="24.957" cy="43.901" r="0.99"/>
                                    <circle fill="#BEBFC0" cx="24.956" cy="43.901" r="0.854"/>
                                    <circle fill="#E6E6E6" cx="24.73" cy="43.673" r="0.148"/>
                                    <circle fill="#E6E6E6" cx="25.182" cy="43.675" r="0.148"/>
                                    <circle fill="#E6E6E6" cx="25.183" cy="44.128" r="0.148"/>
                                    <circle fill="#E6E6E6" cx="24.729" cy="44.128" r="0.147"/>
                                </g>
                                <g>
                                    <circle fill="#BEBFC0" cx="24.956" cy="47.819" r="1.226"/>
                                    <circle fill="#ABADAE" cx="24.957" cy="47.819" r="0.99"/>
                                    <circle fill="#BEBFC0" cx="24.956" cy="47.819" r="0.854"/>
                                    <circle fill="#E6E6E6" cx="24.73" cy="47.592" r="0.148"/>
                                    <circle fill="#E6E6E6" cx="25.182" cy="47.592" r="0.148"/>
                                    <circle fill="#E6E6E6" cx="25.183" cy="48.045" r="0.148"/>
                                    <circle fill="#E6E6E6" cx="24.729" cy="48.046" r="0.149"/>
                                </g>
                                <g>
                                    <path fill="#7CCBB9" d="M35.243,33.667c-2.191,0.06-4.3,0.448-6.279,1.118c-1.391,0.473-2.716,1.081-3.963,1.815
                                        c-1.246-0.734-2.572-1.342-3.964-1.814c-1.978-0.67-4.086-1.059-6.276-1.119c-0.382,1.105-0.591,2.293-0.591,3.529
                                        s0.209,2.423,0.591,3.529c2.19-0.061,4.298-0.448,6.276-1.119c1.392-0.472,2.718-1.082,3.964-1.814
                                        c1.247,0.732,2.572,1.343,3.963,1.815c1.979,0.67,4.088,1.058,6.279,1.118c0.381-1.106,0.59-2.293,0.59-3.529
                                        S35.624,34.772,35.243,33.667z"/>
                                    <path fill="#71B9AA" d="M25.001,36.6c-1.214-0.715-2.506-1.311-3.859-1.778c-0.429,0.688-0.678,1.502-0.678,2.374
                                        s0.249,1.685,0.678,2.374c1.354-0.468,2.645-1.063,3.859-1.778c1.192,0.701,2.459,1.289,3.786,1.752
                                        c0.42-0.683,0.662-1.487,0.662-2.348s-0.242-1.665-0.662-2.349C27.46,35.311,26.193,35.898,25.001,36.6z"/>
                                    <circle fill="#7CCBB9" cx="24.956" cy="37.196" r="3.02"/>
                                </g>
                                <g>
                                    <path fill="#434854" d="M25.019,14.012c0,0-1.266-1.427-3.563-1.127c-3.103,0.406-3.525,3.059-3.782,3.64
                                        c-0.26,0.581-0.814,1.075-1.409,1.036c-0.505-0.034-0.991-0.363-0.943-1.006c0.031-0.419,0.359-0.637,0.761-0.586
                                        c0.38,0.05,0.431,0.564,0.171,0.683c0,0,0.317-0.126,0.324-0.544c0.002-0.162-0.064-0.419-0.349-0.568
                                        c-0.822-0.43-1.996,0.38-1.996,1.586c0,0.949,0.872,4.283,5.843,3.513C25.047,19.867,25.364,15.584,25.019,14.012z"/>
                                    <path fill="#434854" d="M24.983,14.012c0,0,1.266-1.427,3.564-1.127c3.103,0.406,3.524,3.059,3.783,3.64
                                        c0.258,0.581,0.812,1.075,1.407,1.036c0.506-0.034,0.992-0.363,0.943-1.006c-0.032-0.419-0.36-0.637-0.761-0.586
                                        c-0.381,0.05-0.433,0.564-0.173,0.683c0,0-0.315-0.126-0.323-0.544c-0.003-0.162,0.063-0.419,0.35-0.568
                                        c0.82-0.43,1.993,0.38,1.993,1.586c0,0.949-0.87,4.283-5.841,3.513C24.955,19.867,24.638,15.584,24.983,14.012z"/>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
            </section>
            <section class="toolbar">
                @if(Auth::check())
                <h1 class="title">{{ Auth::user()->name }}</h1>
                <div class="btn-group options">
                  <button class="btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-2x fa-sliders" aria-hidden="true"></i>
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <li><a target="_blank" href="{{ url('/#/start') . '/' . Auth::user()->id }}">View store</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ url('/bar') }}">Settings</a></li>
                    <li><a href="{{ url('/logout') }}">Log out</a></li>
                  </ul>
                </div>
                @endif
            </section>
        </header>

        <main id="main">
            <nav id="main-nav">
                <ul class="c-tab-nav">
                    <li id="tab-drinks" class="c-tab {{ $activeTab == 'drinks' ? 'is-active' : '' }}">
                        <a href="{{ route('categories.index') }}">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="100%" width="100%" viewBox="0 0 30 30" enable-background="new 0 0 30 30" xml:space="preserve">
                                <g>
                                    <g>
                                        <path class="grey" fill="#939597" d="M29.893,6.711c-0.096-0.228-0.314-0.375-0.561-0.375h-16.57c0.002-1.624-0.593-3.156-1.71-4.35
                                            C9.855,0.709,8.163-0.023,6.415-0.023c-1.73,0-3.349,0.685-4.555,1.925c-1.208,1.241-1.845,2.88-1.797,4.613
                                            c0.097,3.402,2.949,6.17,6.356,6.17h0.094c1.335-0.021,2.636-0.474,3.703-1.271l0.871,0.874h13.784l4.893-4.914
                                            C29.936,7.2,29.986,6.939,29.893,6.711z"/>
                                        <g>
                                            <polygon class="liquid" fill="#404041" points="24.871,12.288 11.087,12.288 17.291,18.52 18.668,18.52 			"/>
                                            <polygon class="grey" fill="#939597" points="18.668,18.52 17.291,18.52 17.58,18.812 17.58,29.205 13.512,29.205 13.512,30 22.445,30
                                                22.445,29.205 18.377,29.205 18.377,18.812 			"/>
                                        </g>
                                    </g>
                                    <circle class="orange" fill="#58585B" cx="6.412" cy="6.332" r="6.35"/>
                                    <g>
                                        <path class="grey" fill="#939597" d="M29.893,6.711c-0.096-0.228-0.314-0.375-0.561-0.375H17.979v5.952h6.893l4.893-4.914
                                            C29.936,7.2,29.986,6.939,29.893,6.711z"/>
                                    </g>
                                    <g>
                                        <path class="grey" fill="#939597" d="M6.064,6.711c0.095-0.228,0.315-0.375,0.562-0.375h11.353v5.952h-6.891L6.195,7.375
                                            C6.021,7.2,5.97,6.939,6.064,6.711z"/>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    </li>
                    <li id="tab-cart" class="c-tab {{ $activeTab == 'cart' ? 'is-active' : '' }}">
                        <a href="{{ route('orders.show') }}">
                            <span class="count-wrapper">
                                <span class="count" ng-show="app.new">🌜 app.new 🌛</span>
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="100%" viewBox="0 0 30 30" enable-background="new 0 0 30 30" xml:space="preserve">
                                    <g>
                                        <path class="dark" fill="#58585B" d="M24.003,24.003c-1.649,0-3.006,1.347-3.006,2.993c0,1.656,1.356,3.004,3.006,3.004
                                            c1.646,0,2.992-1.348,2.992-3.004C26.995,25.35,25.649,24.003,24.003,24.003z"/>
                                        <path class="light" fill="#6D6E70" d="M9.002,24.003c-1.647,0-3.004,1.347-3.004,2.993C5.998,28.652,7.354,30,9.002,30s2.995-1.348,2.995-3.004
                                            C11.997,25.35,10.649,24.003,9.002,24.003z"/>
                                        <path class="light" fill="#6D6E70" d="M9.302,19.198v-0.149l1.347-2.547h5.771V3.003H6.297L4.952,0H0v3.003h3.003L8.4,14.4l-2.104,3.603
                                            c-0.144,0.446-0.299,1.046-0.299,1.493c0,1.656,1.356,3.004,3.004,3.004h7.418v-3.004H9.604
                                            C9.449,19.496,9.302,19.351,9.302,19.198z"/>
                                        <rect class="dark" x="16.42" y="19.496" fill="#58585B" width="10.575" height="3.004"/>
                                        <path class="dark" fill="#58585B" d="M21.753,16.502c1.195,0,2.097-0.602,2.55-1.502l5.396-9.747C30,4.952,30,4.797,30,4.497
                                            c0-0.893-0.6-1.493-1.503-1.493H16.42v13.499H21.753z"/>
                                    </g>
                                </svg>
                            </span>
                        </a>
                    </li>
                    <li id="tab-account" class="c-tab {{ $activeTab == 'account' ? 'is-active' : '' }}">
                        <a href="{{ route('bar.index') }}">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 43.621 53.691" enable-background="new 0 0 43.621 53.691" xml:space="preserve">
                                <g>
                                	<g>
                                		<path class="head" fill="#E4E4E4" d="M12.596,33.253c1.876-1.208,1.666-4.802,1.666-5.641H29.36c0,0.839-0.231,4.434,1.644,5.641L21.812,43.55
                                			L12.596,33.253z"/>
                                		<path class="head-shadow" fill="#CECECE" d="M27.684,36.839l3.319-3.586c-1.875-1.208-1.643-4.803-1.643-5.641h-15.1
                                			C15.345,30.726,22.509,36.07,27.684,36.839L27.684,36.839z"/>
                                		<path class="head" fill="#E4E4E4" d="M33.555,16.708c0,6.717-3.868,15.099-11.744,15.099s-11.743-8.382-11.743-15.099
                                			c0-6.718,5.258-9.229,11.743-9.229C28.296,7.479,33.555,9.99,33.555,16.708L33.555,16.708z"/>
                                		<path class="account-lightest" fill="#F2F2F2" d="M41.104,53.616c1.39,0,2.517-1.126,2.517-2.517v-8.09c0-3.69-2.877-7.631-6.392-8.756l-5.249-1.68
                                			c-2.822,2.159-6.857,5.104-10.169,5.104c-3.312,0-7.347-2.944-10.171-5.104l-5.248,1.68c-3.515,1.125-6.391,5.064-6.391,8.756
                                			v8.09c0,1.391,1.126,2.517,2.517,2.517H41.104z"/>
                            		<g>
                            			<g>
                            				<defs>
                            					<rect id="SVGID_1_" x="0.002" y="-0.077" width="43.635" height="53.847"/>
                            				</defs>
                            				<clipPath id="SVGID_2_">
                            					<use xlink:href="#SVGID_1_"  overflow="visible"/>
                            				</clipPath>
                            				<path clip-path="url(#SVGID_2_)" class="account-dark" fill="#555566" d="M16.779,2.447H15.1c-3.243,0-5.872,2.659-5.872,5.902
                            					c0,3.011,0.839,8.358,0.839,8.358s0.944-4.248,5.032-5.873C17.6,9.842,16.779,2.447,16.779,2.447L16.779,2.447z"/>
                            			</g>
                            			<g>
                            				<defs>
                            					<rect id="SVGID_3_" x="0.002" y="-0.077" width="43.635" height="53.847"/>
                            				</defs>
                            				<clipPath id="SVGID_4_">
                            					<use xlink:href="#SVGID_3_"  overflow="visible"/>
                            				</clipPath>
                            				<path clip-path="url(#SVGID_4_)" class="account-dark" fill="#555566" d="M15.939,2.477c3.373-5.186,12.059-0.868,17.616-0.868
                            					c0.838,1.205,1.416,10.328,0,15.1l-2.517-4.194c0,0-7.689,2.825-15.938-1.647"/>
                            			</g>
                            		</g>
                            		<path class="account-light" fill="#E0E0E0" d="M21.812,37.677l-4.194,4.194l-6.711-9.228l1.954-1.954c0.315-0.315,0.823-0.329,1.154-0.03L21.812,37.677z
                            			"/>
                            		<path class="account-light" fill="#E0E0E0" d="M21.812,37.677l4.193,4.194l6.711-9.228l-1.954-1.954c-0.315-0.315-0.822-0.329-1.154-0.03L21.812,37.677z
                            			"/>
                            		<path class="account-dark" fill="#555566" d="M10.967,32.79l-4.254,1.361v19.434h12.582L10.967,32.79z"/>
                            		<path class="account-dark" fill="#555566" d="M32.655,32.79l4.255,1.361v19.434H24.328L32.655,32.79z"/>
                            		<path class="account-dark" fill="#555566" d="M23.489,40.194c0,0.462-0.377,0.839-0.839,0.839h-1.678c-0.461,0-0.838-0.377-0.838-0.839v-1.678
                            			c0-0.461,0.377-0.84,0.838-0.84h1.678c0.461,0,0.839,0.378,0.839,0.84V40.194z"/>
                            		<path class="account-dark" fill="#555566" d="M20.972,38.517L16.271,36.5c-0.554-0.235-1.17,0.17-1.17,0.771v4.167c0,0.602,0.616,1.008,1.17,0.77
                            			l4.702-2.015L20.972,38.517L20.972,38.517z"/>
                            		<path class="account-dark" fill="#555566" d="M22.65,38.517l4.703-2.017c0.555-0.235,1.169,0.17,1.169,0.771v4.167c0,0.602-0.614,1.008-1.169,0.77
                            			l-4.703-2.015V38.517z"/>
                            		<path class="account-light" fill="#E0E0E0" d="M22.65,45.227c0,0.464-0.375,0.839-0.838,0.839c-0.464,0-0.839-0.375-0.839-0.839
                            			c0-0.463,0.375-0.838,0.839-0.838C22.274,44.388,22.65,44.764,22.65,45.227L22.65,45.227z"/>
                            		<path class="account-light" fill="#E0E0E0" d="M22.65,49.421c0,0.463-0.375,0.838-0.838,0.838c-0.464,0-0.839-0.375-0.839-0.838s0.375-0.839,0.839-0.839
                            			C22.274,48.583,22.65,48.958,22.65,49.421L22.65,49.421z"/>
                            		<path class="head" fill="#E4E4E4" d="M10.472,18.385C9.214,18.07,8.39,18.742,8.39,20.062c0,1.748,1.691,6.028,3.469,3.355
                            			C13.636,20.744,10.472,18.385,10.472,18.385L10.472,18.385z"/>
                            		<path class="head" fill="#E4E4E4" d="M32.96,18.385c1.373-0.314,2.271,0.357,2.271,1.678c0,1.748-1.845,6.028-3.786,3.355
                            			C29.505,20.744,32.96,18.385,32.96,18.385L32.96,18.385z"/>
                            	       </g>
                                </g>
                            </svg>
                        </a>
                    </li>
                </ul>
            </nav>

            <section id="main-content">
                @yield('content')
            </section>
        </main>

    </div>

    <!-- Custom -->
    @yield('scripts')
</body>
</html>

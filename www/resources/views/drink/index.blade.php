@extends('layouts.backoffice', [ 'activeTab' => 'drinks' ])

@section('content')
<div class="container" style="padding: 20px">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
			<!-- will be used to show any messages -->
			@if (Session::has('message'))
				<div class="alert alert-info">{{ Session::get('message') }}</div>
			@endif

            <h1>Dranken <a class="btn btn-default" href="{{ url('/drinks/create') }}"><i class="glyphicon glyphicon-plus"></i></a></h1>

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<td>ID</td>
						<td>Name</td>
						<td>Description</td>
						<td>Price</td>
						<td>Actions</td>
					</tr>
				</thead>
				<tbody>
				@if ($drinks == null || count($drinks) < 1)
					<tr>
						<td>I don't have any record!</td>
					</tr>
				@else
				@foreach ($drinks as $drink)
					<tr>
						<td>{{ $drink->id }}</td>
						<td>{{ $drink->name }}</td>
						<td>{{ $drink->description }}</td>
						<td>
							@if($drink->promoted)
							<strike>{{ $drink->price }}</strike> {{ $drink->promoted_price }}
							@else
							{{ $drink->price }}
							@endif
						</td>
						<td>
							{!! Form::open([
								'method' => 'DELETE',
								'route' => ['drinks.destroy', $drink->id]
							]) !!}
								<a class="btn btn-small btn-primary" href="{{ URL::to('drinks/' . $drink->id . '/edit')}}">Edit</a>
								{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
							{!! Form::close() !!}
						</td>
					</tr>
				@endforeach
				@endif
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

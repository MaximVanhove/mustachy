@extends('layouts.backoffice', [ 'activeTab' => 'drinks' ])

@section('content')
<div class="container" style="padding: 20px">
    <div class="row">
        <div class="col-xs-12">

            @if(null !== Session::get('error'))
                <div class="alert alert-warning">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Er is iet fout!</strong> {{ Session::get('error') }}
                </div>
            @endif

			{!! Form::open(array('url' => 'drinks')) !!}
			<div class="form-group">
				{!! Form::label('name', 'Name:') !!}
				{!! Form::text('name',null,['class'=>'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('price', 'Price:') !!}
				{!! Form::number('price',null,['class'=>'form-control','step'=>'0.01']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('promoted', 'Promoted:') !!}
				{!! Form::checkbox('promoted',null,null) !!}
			</div>
			<div class="form-group">
				{!! Form::label('promoted_price', 'Promoted price:') !!}
				{!! Form::number('promoted_price',null,['class'=>'form-control','step'=>'0.01']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('description', 'Description:') !!}
				{!! Form::textarea('description',null,['class'=>'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('category', 'Category:') !!}
				{!! Form::select('category',$categories,$categoryId, ['class'=>'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection

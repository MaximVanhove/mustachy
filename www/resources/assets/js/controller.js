;(function () {
    'use strict';

    // Module declarations
    angular.module('app')
        .controller('AppController', AppController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    AppController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$interval',
        '$filter',
        // Services
        'Order'
    ];

    function AppController(
        // Angular
        $log,
        $scope,
        $interval,
        $filter,
        // Services
        Order
    ) {
        // ViewModel
        // =========
        var app = this;

        // User Interface
        // --------------

        app.new = 0;

        function refresh(){
            var orders = Order.query(function () {
                var newOrders = $filter('filter')(orders, { status: 0 });
                app.new = newOrders.length;
            });
        }

        var promise = $interval(refresh, 1000);

        // Cancel interval on page changes
        $scope.$on('$destroy', function(){
            if (angular.isDefined(promise)) {
                $interval.cancel(promise);
                promise = undefined;
            }
        });

    }

})();

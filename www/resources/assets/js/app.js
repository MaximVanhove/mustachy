;(function () {
    'use strict';

    var app = angular.module('app', [
        // Angular Modules
        // ---------------
        'ngResource'
    ]);

    app.config(function ($interpolateProvider) {

        $interpolateProvider.startSymbol('🌜');
        $interpolateProvider.endSymbol('🌛');

    });

})();

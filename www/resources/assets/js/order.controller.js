;(function () {
    'use strict';

    // Module declarations
    angular.module('app')
        .controller('OrderController', OrderController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    OrderController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        '$interval',
        // Services
        'Order',
        'CSRF_TOKEN'
    ];

    function OrderController(
        // Angular
        $log,
        $scope,
        $http,
        $interval,
        // Services
        Order,
        CSRF_TOKEN
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'orders'
        }

        vm.orders = Order.query();
        vm.ordercount = null;

        vm.move = function (order) {
            order.status++;
            order.$update({ id: order.id }, function() {
                refresh();
            });
        }

        var order = Order.get({id:1}, function () {
            console.log(order);
        });

        function refresh(){
            var orders = Order.query(function () {
                vm.orders = orders;
            });
        }

        var promise = $interval(refresh, 10000);

        // Cancel interval on page changes
        $scope.$on('$destroy', function(){
            if (angular.isDefined(promise)) {
                $interval.cancel(promise);
                promise = undefined;
            }
        });

    }

    angular.module('app').factory('Order', function($resource, CSRF_TOKEN) {
        return $resource('/backoffice/api/v1/orders/:id', { id: '@_id' }, {
            update: {
                method: 'PUT', // this method issues a PUT request
            }
        });
    });

})();

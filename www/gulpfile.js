var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass(['main.scss'], 'public/css/main.css');
    mix.scripts([
        '../../../node_modules/angular/angular.js',
        '../../../node_modules/angular-resource/angular-resource.js',
        'app.js',
        'controller.js',
        'order.controller.js'
    ], 'public/js/app.js');
    // mix.browserSync({
    //     proxy: 'www.mustachy.dev'
    // });
});

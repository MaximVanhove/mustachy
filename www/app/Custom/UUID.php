<?php

namespace App\Custom;

class UUID {

    public function __construct()
    {
        $chars = array();
        $index = 1;
        $char = 48; // ascii 0

        for ($char; $char <= 122; $char++) {
            $chars = array_add($chars, $index, $char);
            if($char == 57) { $char = 65; } // char 65 = ascii A
            if($char == 90) { $char = 97; } // char 97 = ascii a
            $index++;
        }

        $uuid = '';

        for ($i=0; $i < 10; $i++) {
            $uuid = $uuid . chr($chars[ rand ( $min = 1 , $max = count($chars) ) ]);
        }

        $this->uuid = $uuid;
    }

    /**
     * Limit the UUID by a number of characters
     *
     * @param $length
     * @param int $start
     * @return $this
     */
    public function limit($length, $start = 0)
    {
        $this->uuid = substr($this->uuid, $start, $length);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->uuid;
    }
}

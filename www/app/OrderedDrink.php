<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderedDrink extends Model
{
    protected $fillable = ['drink_id', 'order_id', 'amount'];

    public function order()
    {
		return $this->belongsTo('App\Order');
    }

    public function drink()
    {
		return $this->hasOne('App\Drink', 'id', 'drink_id');
    }
}

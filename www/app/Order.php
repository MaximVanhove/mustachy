<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'status', 'uuid', 'note'];

    public function user()
    {
		return $this->belongsTo('App\User');
    }

    public function orderedDrinks()
    {
        return $this->hasMany('App\OrderedDrink');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('app.index');
});

Route::group(['prefix' => 'api/v1', 'middleware' => ['cors']], function () {
    Route::get('orders', 'ApiController@index');
    Route::post('order', 'ApiController@store');
    Route::get('order/{order}', 'ApiController@getOrder');
    Route::get('bar/{user}', 'ApiController@getBar');
    Route::get('drinks/{user}', 'ApiController@getDrinks');
    Route::get('categories/{user}', 'ApiController@getCategories');
    Route::get('category/{id}', 'ApiController@getCategory');
    Route::get('popular/{id}', 'ApiController@getPopular');
    Route::get('sales/{id}', 'ApiController@getSales');
});

Route::group(['prefix' => 'backoffice/api/v1', 'middleware' => ['web']], function () {
    Route::resource('orders', 'Api\Backoffice\OrderController');
    // Route::resource('bar.orders', 'Api\Backoffice\OrderController', [
    //     'parameters' => 'singular'
    // ]);
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');

    // Backoffice
    Route::resource('categories', 'CategoryController');
    Route::resource('drinks', 'DrinkController');
    Route::resource('bar', 'BarController');
    Route::get('drinks/create/{categoryId}', 'DrinkController@create');
    Route::get('orders/{tab}', 'OrderController@index')->name('orders.index');
    Route::get('orders', 'OrderController@show')->name('orders.show');
    Route::get('orders/create', 'OrderController@create')->name('orders.create');
    Route::patch('orders', 'OrderController@update')->name('orders.update');
});

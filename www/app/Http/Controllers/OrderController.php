<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\Drink;
use App\OrderedDrink;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tab)
    {
        switch ($tab) {
            case 'new':
                $orders = Order::with('orderedDrinks')->where('status', 0)->get();
                $tab = 0;
                $tabname = 'new';
                $tabtitle = 'Nieuwe bestellingen';
                break;

            case 'finished':
                $orders = Order::with('orderedDrinks')->where('status', 1)->get();
                $tab = 1;
                $tabname = 'finished';
                $tabtitle = 'Afgewerkte bestellingen';
                break;

            case 'payed':
                $orders = Order::with('orderedDrinks')->where('status', 2)->get();
                $tab = 2;
                $tabname = 'payed';
                $tabtitle = 'Betaalde bestellingen';
                break;

            case 'archive':
                $orders = Order::with('orderedDrinks')->where('status', 3)->get();
                $tab = 3;
                $tabname = 'archive';
                $tabtitle = 'Prullenmand';
                break;

            default:
                $orders = Order::with('orderedDrinks')->where('status', '!=', 4)->orderBy('status', 'ASC')->get();
                $tab = -1;
                $tabname = 'all';
                $tabtitle = 'Alle bestellingen';
                break;
        }

        foreach ($orders as $key => $order) {
            $tobepayed = 0;
            $totaldrinks = 0;

            foreach ($order->orderedDrinks as $key => $product) {
                $tobepayed += $product->drink->current_price * $product->amount;
                $totaldrinks += $product->amount;
            }

            $order->tobepayed = $tobepayed;
            $order->totaldrinks = $totaldrinks;
        }

        return view('order.index')
            ->with(['orders'    => $orders,
                    'tab'       => $tab,
                    'tabname'   => $tabname,
                    'tabtitle' => $tabtitle
                ]);
    }

    public function show(){
        return view('order.show');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function update(Request $request)
    {
        $input = $request->all();

        $order = Order::findOrFail($input['id']);

        $status = intval($input['status']);

        if ($status >= 0 && $status < 3) {
            $status += 1;
            $order->status = $status;
            $order->save();
        } else {

        }

        return redirect()->action('OrderController@index', [$input['tab']]);
    }
}

<?php

namespace App\Http\Controllers;

use Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Drink;
use App\Category;

class DrinkController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// get all the drinks
        $drinks = Drink::all();

        // load the view and pass the tables
        return view('drink.index')
            ->with('drinks', $drinks);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($categoryId=1)
	{
		// get all the categories
		$categories = Category::lists('name', 'id');

		// load the view and pass the categories
		return view('drink.create')
			->with(['categories' => $categories, 'categoryId' => $categoryId]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	 public function store(Request $request)
	 {
		$input = $request->input();
		$user = Auth::user();

		$this->validate($request, [
	        'name' => 'required',
	        'price' => 'required'
	    ]);

        if($request->get('price') <= $request->get('promoted_price')){
            $error = "De promo prijs moet lager zijn dan de originele prijs.";
            return redirect('drinks/create')->with('error', $error);
        }

		if($request->get('price') < 0){
            $error = "De prijs moet hoger zijn dan nul.";
            return redirect('drinks/create')->with('error', $error);
        }

		if($request->get('promoted_price') < 0){
            $error = "De promotie prijs moet hoger zijn dan nul.";
            return redirect('drinks/create')->with('error', $error);
        }

		$categoryId = $request->get('category');
		$category = Category::where('id', $categoryId)->where('user_id', $user->id)->first();

		if($category == null){
			$error = "Kies een categorie";
			return redirect()->back->with('error', $error);
		}

		$input = $request->all();

		// Checkbox bug fix
		$input['promoted'] = ( isset($input['promoted']) ) ? '1' : '0';

        $input['category_id'] = $category->id;
        $input['user_id'] = $user->id;

		$drink = Drink::create($input);

		return redirect('categories/'.$categoryId);
	 }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$drink = Drink::findOrFail($id);
		return $drink;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$categories = Category::lists('name', 'id');
		$user = Auth::user();

		$drink = Drink::findOrFail($id);

		if($drink->user_id != $user->id) return response()->view('errors.404', [], 404);

		return view('drink.edit')->with(['drink' => $drink,'categories' => $categories]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$drink = Drink::findOrFail($id);
		$user = Auth::user();

		if($drink->user_id != $user->id) return response()->view('errors.404', [], 404);

	    $this->validate($request, [
	        'name' => 'required',
	        'price' => 'required'
	    ]);

        if($request->get('price') <= $request->get('promoted_price')){
            $error = "De promo prijs moet lager zijn dan de originele prijs";
            return redirect('drinks/'.$drink->id.'/edit')->with('error', $error);
        }

	    $input = $request->all();

		// Checkbox bug fix
		$input['promoted'] = ( isset($input['promoted']) ) ? '1' : '0';

		$categoryId = $request->get('category');
		$category = Category::findOrFail($categoryId);

	    $drink->fill($input)->save();
		$category->drinks()->save($drink);

	    return redirect('categories/'.$categoryId);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$drink = Drink::findOrFail($id);
        $category = $drink->category()->first();
		$user = Auth::user();

		if($drink->user_id != $user->id) return response()->view('errors.404', [], 404);

		$drink->delete();

		return redirect('categories/'.$category->id);
	}
}

<?php

namespace App\Http\Controllers\Api\Backoffice;

use Illuminate\Http\Request;

use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bar = Auth::user();
        $orders = $bar->orders()->with('orderedDrinks')->with('orderedDrinks.drink')->get();

        foreach ($orders as $key => $order) {
            $tobepayed = 0;
            $totaldrinks = 0;

            foreach ($order->orderedDrinks as $key => $product) {
                $tobepayed += $product->drink->current_price * $product->amount;
                $totaldrinks += $product->amount;
            }

            $order->tobepayed = $tobepayed;
            $order->totaldrinks = $totaldrinks;
        }

        return $orders;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $order = Order::findOrFail($input['id']);
        $order->fill($input);
        $order->save();
        return $order;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with('orderedDrinks')->with('orderedDrinks.drink')->findOrFail($id);

        $tobepayed = 0;
        $totaldrinks = 0;

        foreach ($order->orderedDrinks as $key => $product) {
            $tobepayed += $product->drink->current_price * $product->amount;
            $totaldrinks += $product->amount;
        }

        $order->tobepayed = $tobepayed;
        $order->totaldrinks = $totaldrinks;

        return $order;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $order = Order::findOrFail($id);
        $order->fill($input);
        $order->save();
        return $order;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Custom\UUID;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\Drink;
use App\OrderedDrink;
use App\Category;
use App\User;

class ApiController extends Controller
{
    /**
     * Get all orders
     *
     * @return JSON
     */
    public function index()
    {
        $orders = Order::with('orderedDrinks')->get();
        return $orders->toArray();
    }

    /**
     * Get an order
     *
     * @return JSON
     */
    public function getOrder($id)
    {
        $order = Order::findOrFail($id);
        return $order;
    }

    /**
     * Get a bar
     *
     * @return JSON
     */
    public function getBar($id)
    {
        $bar = User::findOrFail($id);
        return $bar;
    }

    /**
    * Create an order.
    *
    * @return JSON
    */
    public function store(Request $request)
    {
        $input = $request->all();

        $user_id = $input['bar_id'];

        $uuid = new UUID();

        // create order
        $order = Order::create([
            'uuid' => $uuid,
            'user_id' => $user_id,
            'status' => 0,
            'note' => $input['note']
        ]);

        // create orderedDrink for each cart item
        foreach ($input['cart'] as $key => $value) {
            $drink = null;
            $drink = Drink::find($value['item']['id']);
            if($drink != null){
                $orderedDrink = OrderedDrink::create([
                    'order_id' => $order->id,
                    'drink_id' => $drink->id,
                    'amount' => $value['amount']
                ]);
                $orderedDrink->save();
                $order->orderedDrinks()->save($orderedDrink);
            }
        }

        $order->save();

        return $order;
    }

    /**
     * Get the drinks from a category
     *
     * @return JSON
     */
    public function getCategory($id)
    {
        $category = Category::with('drinks')->findOrFail($id);
        return $category->toArray();
    }

    /**
     * Get all categories from bar with id
     *
     * @return JSON
     */
    public function getCategories($id)
    {
        $user = User::findOrFail($id);
        return $user->categories()->with('drinks')->get()->toArray();
    }

    /**
     * Get all drinks from bar with id
     *
     * @return JSON
     */
    public function getDrinks($id)
    {
        $user = User::findOrFail($id);
        return $user->drinks->toArray();
    }

    /**
     * Get all categories from bar with id
     *
     * @return JSON
     */
    public function getPopular($id)
    {
        $user = User::findOrFail($id);
        $drinks = $user->drinks()->orderBy('buyed', 'DESC')->take(5)->get();
        return $drinks;
    }

    /**
     * Get all categories from bar with id
     *
     * @return JSON
     */
    public function getSales($id)
    {
        $user = User::findOrFail($id);
        $drinks = $user->drinks()->where('promoted', 1)->orderBy('created_at', 'DESC')->take(10)->get();
        return $drinks;
    }
}

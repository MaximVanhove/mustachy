<?php

namespace App\Http\Controllers;

use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CategoryController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		/**
		 * Get the first category and show it
		 */
		$category = \Auth::user()->categories()->first();
		return redirect()->action('CategoryController@show', [$category->id]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('category.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// TODO: category validation
		$this->validate($request, [
			'name' => 'required',
			'image' => 'required',
		]);

		$input = $request->all();
        $input['user_id'] = Auth::user()->id;
		$category = Category::create($input);

		if($request->hasFile('image')) {
            $file = Input::file('image');
            $extension = Input::file('image')->getClientOriginalExtension();

            $name = 'category_' . $category->id . '.' . $extension;

            $category->image = \App::make('url')->to('/') . '/images/' . $name;

            $file->move(public_path().'/images/', $name);
        }

        $category->save();

		return redirect()->action('CategoryController@show', [$category->id]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$category = Category::findOrFail($id);

		$user = Auth::user();
		if($category->user_id != $user->id) return response()->view('errors.404', [], 404);

		$drinks = $category->drinks()->get();
		$categories = \Auth::user()->categories()->get();
		return view('category.show')
            ->with([ 'categories'=>$categories, 'category'=>$category, 'drinks'=>$drinks ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = Category::findOrFail($id);

		$user = Auth::user();
		if($category->user_id != $user->id) return response()->view('errors.404', [], 404);

		return view('category.edit')->with('category', $category);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$category = Category::findOrFail($id);

		$user = Auth::user();
		if($category->user_id != $user->id) return response()->view('errors.404', [], 404);

	    $this->validate($request, [
	        'name' => 'required'
	    ]);

		if($request->hasFile('image')) {
            $file = Input::file('image');
            $extension = Input::file('image')->getClientOriginalExtension();

            $name = 'category_' . $category->id . '.' . $extension;

            $category->image = '/images/' . $name;

            $file->move(public_path().'/images/', $name);
        }

	    $category->name = $request->get('name');

		$category->save();

		$drinks = $category->drinks()->get();
		$categories = \Auth::user()->categories()->get();

		return view('category.show')
            ->with([
				'categories'	=> $categories,
				'category'		=> $category,
				'drinks'		=> $drinks,
				'success' 		=> 'Categorie succesvol geupdated'
			]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$item = Category::findOrFail($id);

		$user = Auth::user();
		if($item->user_id != $user->id) return response()->view('errors.404', [], 404);

		$item->delete();

		return redirect('categories');
	}
}

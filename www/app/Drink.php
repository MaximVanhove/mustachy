<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drink extends Model
{
    protected $fillable = ['name','description','price','promoted','promoted_price', 'category_id', 'user_id'];

    protected $appends = ['current_price'];

	public function category()
    {
		return $this->belongsTo('App\Category');
    }

    public function orderedDrinks()
    {
		return $this->belongsToMany('App\OrderedDrink');
    }

    /**
     * Get the current drink price, check if promoted
     *
     * @return int
     */
    public function getCurrentPriceAttribute()
    {
        if ($this->attributes['promoted']) return $this->attributes['promoted_price'];
        return $this->attributes['price'];
    }
}

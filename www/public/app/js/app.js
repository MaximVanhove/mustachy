;(function () {
    'use strict';

    // Module declarations
    angular.module('app', [
        // Angular Modules
        // ---------------
        'ngAnimate',
        'ngCookies',
        //'ngMaterial',
        'ngMessages',
        'ngResource',
        'ui.router',

        // Modules
        // -------
        'app.cart',
        'app.drinks',
        'app.home',
        'app.master',
        'app.responses',
    ]);

    angular.module('app.cart', []);
    angular.module('app.drinks', []);
    angular.module('app.home', []);
    angular.module('app.master', []);
    angular.module('app.responses', []);
})();


/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app')
        .config(Config);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Config.$inject = [
        // Angular
        '$compileProvider'
    ];

    function Config(
        // Angular
        $compileProvider
    ) {
        var debug = true; // Set to `false` for production
        $compileProvider.debugInfoEnabled(debug);

        var colour = {
            amber: 'amber',
            blue: 'blue',
            blueGrey: 'blue-grey',
            brown: 'brown',
            cyan: 'cyan',
            deepOrange: 'deep-orange',
            deepPurple: 'deep-purple',
            green: 'green',
            grey: 'grey',
            indigo: 'indigo',
            lightBlue: 'light-blue',
            lightGreen: 'light-green',
            lime: 'lime',
            orange: 'orange',
            pink: 'pink',
            purple: 'purple',
            red: 'red',
            teal: 'teal',
            yellow: 'yellow'
        };
    }
})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app')
        .constant('CONFIG', {});
})();

;(function () {
    'use strict';

    // Module declarations
    var app = angular.module('app')
        .controller('AppController', AppController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    AppController.$inject = [
        // Angular
        '$location',
        '$log',
        // Other
        '$state',
        'Shop'
    ];

    function AppController(
        // Angular
        $location,
        $log,
        // Other
        $state,
        Shop
    ) {
        // ViewModel
        // =========
        var app = this;


        var regex = new RegExp("\#\/start\/[0-9]*");
        var start = regex.test(window.location.hash);

        if(!start){
            var success = Shop.init();
            if(success == false){
                $location.path('oops').replace();
            }
        }



        // User Interface
        // --------------
        app.$$ui = {
            state : function(name){
                return $state.includes(name);
            }
        }

        app.cart = Shop.cart;
    }
})();

/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$urlRouterProvider'
    ];

    function Routes(
        // Angular
        $urlRouterProvider
    ) {
        $urlRouterProvider.otherwise('/');
    }

})();

;(function () {
    'use strict';

    angular.module('app.cart')
        .controller('CartController', CartController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    CartController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        // Other
        'Shop'
    ];

    function CartController(
        // Angular
        $log,
        $scope,
        $http,
        // Other
        Shop
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Cart'
        }

        vm.cart = Shop.cart;
        vm.product = Shop.product;
        vm.checkout = { note: '' };

        vm.order = function(){
            Shop.order(vm.checkout.note);
        };
    }

})();

;(function () {
    'use strict';

    angular.module('app.cart')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('app.cart', {
                cache: false, // false will reload on every visit.
                controller: 'CartController as vm',
                templateUrl: '/app/html/cart/cart.view.html',
                url: '/cart'
            });
    }

})();

;(function () {
    'use strict';

    angular.module('app.drinks')
        .controller('DrinksController', DrinksController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    DrinksController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        '$state',
        // Other
        'Shop'
    ];

    function DrinksController(
        // Angular
        $log,
        $scope,
        $http,
        $state,
        // Other
        Shop
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            categories: Shop.categories,
            category: Shop.category,
            name: $state.params.category
        }

        vm.product = Shop.product;
        vm.settings = {
            sort: '',
            reverse: true
        }
    }

})();

;(function () {
    'use strict';

    angular.module('app.drinks')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('app.drinks', {
                cache: false, // false will reload on every visit.
                controller: 'DrinksController as vm',
                templateUrl: '/app/html/drinks/categories.view.html',
                url: '/drinks'
            })
            .state('app.drinks-list', {
                cache: false, // false will reload on every visit.
                controller: 'DrinksController as vm',
                templateUrl: '/app/html/drinks/drinks.view.html',
                url: '/drinks/:category'
            });
    }

})();

;(function () {
    'use strict';

    angular.module('app')
        .factory('Shop', Shop);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Shop.$inject = [
        /// Angular
        '$cookies',
        '$http',
        '$state',
        '$timeout',
        '$httpParamSerializerJQLike',
        'URL'
    ];

    function Shop($cookies, $http, $state, $timeout, $httpParamSerializerJQLike, URL) {

        /*------------------------------------------*\
                         Properties
        \*------------------------------------------*/
        // TODO: init on start or get from cookies
        var bar_id = null;
        var bar;

        var sales = {loading: true};
        var popular = {loading: true};
        var categories = {loading: true};
        var products = {loading: true};
        var orders = new Array();

        var cart = {
            items: new Array(),
            add: function(){},
            total: function(){
                var total = 0;
                this.items.forEach(function(product) {
                    total += ( product.item.current_price * product.amount );
                });
                return total;
            },
            checkout: function(){},
            note: '',
            count : 0
        }

        /*------------------------------------------*\
                           Objects
        \*------------------------------------------*/
        function product(item){
          this.item = item;
          this.amount = 0;
          this.add = function(){
            this.amount++;
            cart.count++;
            if(-1 == cart.items.indexOf(this)) cart.items.push(this);
          };
          this.subtract = function(){
            if(this.amount > 1) {
              this.amount--;
              cart.count--;
            }
            else if(this.amount == 1){
              cart.items.splice(cart.items.indexOf(this), 1);
              this.amount = 0;
              cart.count--;
            }
          };
        }

        /*------------------------------------------*\
                          Functions
        \*------------------------------------------*/
        function initApp(){
            var bar = $cookies.getObject('bar_id');
            if (bar != null && bar != "undefined") {
                bar_id = bar.id;
                getData();
                return true;
            }
            return false;
        }

        function getData(){
            $http.get(URL + '/api/v1/bar/' + bar_id)
            .success(function(data) {
                bar = data;
            });

            $http.get(URL + '/api/v1/sales/' + bar_id)
            .success(function(data) {
                sales = data;
            });

            $http.get(URL + '/api/v1/popular/' + bar_id)
            .success(function(data) {
                popular = data;
            });

            $http.get(URL + '/api/v1/categories/' + bar_id)
            .success(function(data) {
                // Create array with name as key for easy access
                var load = {};
                for (var i = 0; i < data.length; i++) {
                    load[data[i].name] = data[i];
                }
                categories = load;
            });

            $http.get(URL + '/api/v1/drinks/' + bar_id)
            .success(function(data) {
                // Create array with id as key for easy access
                var load = {};
                for (var i = 0; i < data.length; i++) {
                    load[data[i].id] = new product(data[i]);
                }
                products = load;
            });
        }

        function updateOrders(){
            orders.forEach(function(order){
                updateOrder(order);
            });
        }

        function updateOrder(order){
            $http.get(URL + '/api/v1/order/' + order.id)
            .success(function(data) {
                orders.forEach(function(item){
                    if(item.id == data.id){
                        var index = orders.indexOf(item);
                        if (index !== -1) {
                            if(data.status > 2){
                                orders.splice(index, 1);
                            }
                            else{
                                orders[index] = data;
                            }
                        }
                    }
                });
            });
        }

        function order(note){
            var request = {
                bar_id: bar_id,
                cart: cart.items,
                note: note
            };

            var response = $http({
                method: 'POST',
                url: URL + '/api/v1/order',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $httpParamSerializerJQLike(request)
            }).success(orderSuccess).error(orderError);
        }

        function orderError(data){
          console.log('error!');
        }

        function orderSuccess(order){
            // Go to the succes page
            $state.go('app.success');

            // If app is still at the succes page after 2 sec, go to dashboard
            $timeout(function(){
                if($state.includes('app.success')) $state.go('app.home');
            }, 2000);
            
            // Empty the cart
            cart.items = [];
            cart.count = 0;

            // Reset products
            getData();

            orders.push(order);
            console.log(orders);
            // TODO: Add order to user orders in cookies
        }

        //getData();
        initApp();

        return {
            init : function(){
                return initApp();
            },
            // Set and get the id of the bar
            setId : function(id){
                bar_id = id;
            },

            getId : function(){
                return bar_id;
            },

            bar : function(){
                return bar;
            },

            // create the order
            order : function(note){
                if(cart.items.length > 0) order(note);
            },

            // get a product
            product : function(id){
                return products[id];
            },

            // get the cart
            cart : function(id){
                return cart;
            },

            sales : function() {
                return sales;
            },

            // get popular
            popular : function() {
                return popular;
            },

            // get categories
            categories : function() {
                return categories;
            },

            category: function(id) {
                return categories[id];
            },

            orders: function() {
                return orders;
            },

            updateOrders: function(){
                updateOrders();
            }
        }

    }

})();

;(function () {
    'use strict';

    angular.module('app.home')
        .controller('HomeController', HomeController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    HomeController.$inject = [
        // Angular
        '$interval',
        '$log',
        '$scope',
        '$http',
        // Other
        'Shop',
        'URL'
    ];

    function HomeController(
        // Angular
        $interval,
        $log,
        $scope,
        $http,
        // Other
        Shop,
        URL
    ) {
        // ViewModel
        // =========
        var vm = this;

        vm.url = URL;

        vm.sales = Shop.sales;
        vm.popular = Shop.popular;
        vm.product = Shop.product;
        vm.orders = Shop.orders;
        vm.bar = Shop.bar;

        function refresh(){
            Shop.updateOrders();
        }

        var promise = $interval(refresh, 1000);

        // Cancel interval on page changes
        $scope.$on('$destroy', function(){
            if (angular.isDefined(promise)) {
                $interval.cancel(promise);
                promise = undefined;
            }
        });

    }

})();

;(function () {
    'use strict';

    angular.module('app.home')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('app.home', {
                cache: false, // false will reload on every visit.
                controller: 'HomeController as vm',
                templateUrl: '/app/html/home/home.view.html',
                url: '/'
            })
            .state('init', {
                cache: false, // false will reload on every visit.
                controller: 'InitController as vm',
                url: '/start/{init_id:int}'
            });
    }

})();

;(function () {
    'use strict';

    angular.module('app.home')
        .controller('InitController', InitController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    InitController.$inject = [
        '$cookies',
        '$state',
        'Shop'
    ];

    function InitController(
        $cookies,
        $state,
        Shop
    ) {
        // ViewModel
        // =========
        var vm = this;

        $cookies.putObject('bar_id', {id:  $state.params.init_id});
        Shop.setId($state.params.init_id);
        Shop.init();
        $state.go('app.home');
    }

})();

;(function () {
    'use strict';

    angular.module('app.master')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('app', {
                cache: false, // false will reload on every visit.
                templateUrl: '/app/html/master/app.view.html'
            });
    }

})();

;(function () {
    'use strict';

    angular.module('app.responses')
        .controller('ResponsesController', ResponsesController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    ResponsesController.$inject = [
        // Angular
        // Other
        'Shop'
    ];

    function ResponsesController(
        // Angular
        Shop
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Oops'
        }
    }

})();

;(function () {
    'use strict';

    angular.module('app.responses')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('error', {
                cache: false, // false will reload on every visit.
                controller: 'ResponsesController as vm',
                templateUrl: '/app/html/responses/oops.view.html',
                url: '/oops'
            })
            .state('app.success', {
                cache: false, // false will reload on every visit.
                controller: 'ResponsesController as vm',
                templateUrl: '/app/html/responses/success.view.html',
                url: '/success'
            });
    }

})();

<!DOCTYPE html>
<html lang="nl" ng-app="app">
    <head>
        <meta charset="utf-8">
        <title>Mustachy</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ url('/app/css/main.css') }}">

        <!-- Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,200,100,500,700,600' rel='stylesheet' type='text/css'>

        <!-- Scripts -->
        <script src="{{ url('/app/vendor/angular/angular.js') }}"></script>
        <script src="{{ url('/app/vendor/faker/faker.min.js') }}"></script>
        <script src="{{ url('/app/vendor/lodash/lodash.min.js') }}"></script>
        <script src="{{ url('/app/js/app.js') }}"></script>
        <script>
          angular.module("app").value('URL', '{{ url('/') }}');
        </script>
    </head>
    <body ng-controller="AppController as app">
        <div ui-view></div>
    </body>
</html>

<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Frisdranken',
            'user_id' => 1,
        ]);
        Category::create([
            'name' => 'Warme dranken',
            'user_id' => 1,
        ]);
        Category::create([
            'name' => 'Wijn',
            'user_id' => 1,
        ]);
        Category::create([
            'name' => 'Bieren van \'t vat',
            'user_id' => 1,
        ]);
        Category::create([
            'name' => 'Trapiesten',
            'user_id' => 1,
        ]);
        Category::create([
            'name' => 'Blonde bieren',
            'user_id' => 1,
        ]);
        Category::create([
            'name' => 'Bruine bieren',
            'user_id' => 1,
        ]);
        Category::create([
            'name' => 'Fruitbieren',
            'user_id' => 1,
        ]);
        Category::create([
            'name' => 'Whiskeys',
            'user_id' => 1,
        ]);
        Category::create([
            'name' => 'Congnac',
            'user_id' => 1,
        ]);
        Category::create([
            'name' => 'Jenevers',
            'user_id' => 1,
        ]);


        Category::create([
            'name' => 'Wodka',
            'user_id' => 2,
        ]);
        Category::create([
            'name' => 'Rum',
            'user_id' => 2,
        ]);
        Category::create([
            'name' => 'Gin',
            'user_id' => 2,
        ]);
        Category::create([
            'name' => 'Shots',
            'user_id' => 2,
        ]);
        Category::create([
            'name' => 'Amoreto',
            'user_id' => 2,
        ]);
        Category::create([
            'name' => 'Cointreau',
            'user_id' => 2,
        ]);
        Category::create([
            'name' => 'R.I.P.',
            'user_id' => 2,
        ]);
        Category::create([
            'name' => 'Malibum',
            'user_id' => 2,
        ]);
        Category::create([
            'name' => 'Tipico Latino',
            'user_id' => 2,
        ]);


        //factory(Category::class, DatabaseSeeder::AMOUNT['MANY'])->create();
    }
}

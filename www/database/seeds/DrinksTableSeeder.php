<?php

use Illuminate\Database\Seeder;
use App\Drink;

class DrinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Drink::class, DatabaseSeeder::AMOUNT['MAX'])->create();
    }
}

<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Flahertys Irish Pub',
            'email' => 'bar@flahertys.be',
            'password' => bcrypt('triesrichclothes'),
        ]);

        DB::table('users')->insert([
            'name' => 'Hollywoord Café',
            'email' => 'jef@hollywoordcafe.be',
            'password' => bcrypt('thinyellowphrase'),
        ]);

        //factory(User::class, DatabaseSeeder::AMOUNT['DEFAULT'])->create();
    }
}

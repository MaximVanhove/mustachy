<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drinks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 64);
			$table->longText('description');
			$table->double('price', 7, 2); //7 digits in total and 2 after the decimal point.
			$table->double('promoted_price', 7, 2); //7 digits in total and 2 after the decimal point.
			$table->boolean('promoted');
            $table->integer('buyed')->unsigned(); // Count of buys
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drinks');
    }
}

<?php

use App\User;
use App\Drink;
use App\Category;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(User::class, function (Faker $faker) : array {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => Hash::make($faker->password($minLength = 6, $maxLength = 20)),
    ];
});

$factory->define(Category::class, function (Faker $faker) : array {
    return [
        'name' => $faker->word,
        'user_id' => User::all()->random()->id,
    ];
});

$factory->define(Drink::class, function (Faker $faker) : array {

    $price = $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 4); // 1.89, 3.34
    $category =  Category::all()->random();
    $user = User::findOrFail($category->user_id);
    $promoted = false;
    $promoted_price = 0;
    if(rand(0, 3) == 0){
        $promoted = true;
        $promoted_price = ($price - $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = ($price/2) ));
    }

    return [
        'name' => $faker->word,
        'description' => $faker->text($maxNbChars = 400),
        'price' => $price,
        'promoted' => $promoted,
        'promoted_price' => $promoted_price,
        'buyed' => $faker->numberBetween($min = 10, $max = 999), // 8567
        'category_id' => $category->id,
        'user_id' => $user->id,
    ];
});

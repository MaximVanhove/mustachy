$().ready(function(){

    /**
     * Event listeners
     */
    $('#toggle-settings').on('click', toggleSettings);
    $('.tabs-list li').on('click', changeTab);

    /**
     * Event handlers
     */
    function toggleSettings(e){
        e.preventDefault();
        $('#x💀x .settings').toggleClass('open');
    }

    function changeTab(e){
        e.preventDefault();
        var tab = $(this).attr('data-tab');
        var $tabs = $(this).closest('.tabs-wrapper').find('.tabs-list li');
        var $tabContents = $(this).closest('.tabs-wrapper').find('.tab-content');

        $tabs.removeClass('active');
        $tabs.each(function(index){
            if($(this).attr('data-tab') == tab) $(this).addClass('active');
        })

        $tabContents.removeClass('active');
        $tabContents.each(function(index){
            if($(this).attr('data-tab') == tab) $(this).addClass('active');
        })
    }



});

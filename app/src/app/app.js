;(function () {
    'use strict';

    // Module declarations
    angular.module('app', [
        // Angular Modules
        // ---------------
        'ngAnimate',
        'ngCookies',
        //'ngMaterial',
        'ngMessages',
        'ngResource',
        'ui.router',

        // Modules
        // -------
        'app.cart',
        'app.drinks',
        'app.home',
        'app.master',
        'app.responses',
    ]);

    angular.module('app.cart', []);
    angular.module('app.drinks', []);
    angular.module('app.home', []);
    angular.module('app.master', []);
    angular.module('app.responses', []);
})();

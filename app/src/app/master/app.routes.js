;(function () {
    'use strict';

    angular.module('app.master')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('app', {
                cache: false, // false will reload on every visit.
                templateUrl: '/app/html/master/app.view.html'
            });
    }

})();

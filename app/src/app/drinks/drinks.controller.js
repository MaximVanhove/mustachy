;(function () {
    'use strict';

    angular.module('app.drinks')
        .controller('DrinksController', DrinksController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    DrinksController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        '$state',
        // Other
        'Shop'
    ];

    function DrinksController(
        // Angular
        $log,
        $scope,
        $http,
        $state,
        // Other
        Shop
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            categories: Shop.categories,
            category: Shop.category,
            name: $state.params.category
        }

        vm.product = Shop.product;
        vm.settings = {
            sort: '',
            reverse: true
        }
    }

})();

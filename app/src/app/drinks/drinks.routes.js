;(function () {
    'use strict';

    angular.module('app.drinks')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('app.drinks', {
                cache: false, // false will reload on every visit.
                controller: 'DrinksController as vm',
                templateUrl: '/app/html/drinks/categories.view.html',
                url: '/drinks'
            })
            .state('app.drinks-list', {
                cache: false, // false will reload on every visit.
                controller: 'DrinksController as vm',
                templateUrl: '/app/html/drinks/drinks.view.html',
                url: '/drinks/:category'
            });
    }

})();

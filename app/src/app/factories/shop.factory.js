;(function () {
    'use strict';

    angular.module('app')
        .factory('Shop', Shop);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Shop.$inject = [
        /// Angular
        '$cookies',
        '$http',
        '$state',
        '$timeout',
        '$httpParamSerializerJQLike',
        'URL'
    ];

    function Shop($cookies, $http, $state, $timeout, $httpParamSerializerJQLike, URL) {

        /*------------------------------------------*\
                         Properties
        \*------------------------------------------*/
        // TODO: init on start or get from cookies
        var bar_id = null;
        var bar;

        var sales = {loading: true};
        var popular = {loading: true};
        var categories = {loading: true};
        var products = {loading: true};
        var orders = new Array();

        var cart = {
            items: new Array(),
            add: function(){},
            total: function(){
                var total = 0;
                this.items.forEach(function(product) {
                    total += ( product.item.current_price * product.amount );
                });
                return total;
            },
            checkout: function(){},
            note: '',
            count : 0
        }

        /*------------------------------------------*\
                           Objects
        \*------------------------------------------*/
        function product(item){
          this.item = item;
          this.amount = 0;
          this.add = function(){
            this.amount++;
            cart.count++;
            if(-1 == cart.items.indexOf(this)) cart.items.push(this);
          };
          this.subtract = function(){
            if(this.amount > 1) {
              this.amount--;
              cart.count--;
            }
            else if(this.amount == 1){
              cart.items.splice(cart.items.indexOf(this), 1);
              this.amount = 0;
              cart.count--;
            }
          };
        }

        /*------------------------------------------*\
                          Functions
        \*------------------------------------------*/
        function initApp(){
            var bar = $cookies.getObject('bar_id');
            if (bar != null && bar != "undefined") {
                bar_id = bar.id;
                getData();
                return true;
            }
            return false;
        }

        function getData(){
            $http.get(URL + '/api/v1/bar/' + bar_id)
            .success(function(data) {
                bar = data;
            });

            $http.get(URL + '/api/v1/sales/' + bar_id)
            .success(function(data) {
                sales = data;
            });

            $http.get(URL + '/api/v1/popular/' + bar_id)
            .success(function(data) {
                popular = data;
            });

            $http.get(URL + '/api/v1/categories/' + bar_id)
            .success(function(data) {
                // Create array with name as key for easy access
                var load = {};
                for (var i = 0; i < data.length; i++) {
                    load[data[i].name] = data[i];
                }
                categories = load;
            });

            $http.get(URL + '/api/v1/drinks/' + bar_id)
            .success(function(data) {
                // Create array with id as key for easy access
                var load = {};
                for (var i = 0; i < data.length; i++) {
                    load[data[i].id] = new product(data[i]);
                }
                products = load;
            });
        }

        function updateOrders(){
            orders.forEach(function(order){
                updateOrder(order);
            });
        }

        function updateOrder(order){
            $http.get(URL + '/api/v1/order/' + order.id)
            .success(function(data) {
                orders.forEach(function(item){
                    if(item.id == data.id){
                        var index = orders.indexOf(item);
                        if (index !== -1) {
                            if(data.status > 2){
                                orders.splice(index, 1);
                            }
                            else{
                                orders[index] = data;
                            }
                        }
                    }
                });
            });
        }

        function order(note){
            var request = {
                bar_id: bar_id,
                cart: cart.items,
                note: note
            };

            var response = $http({
                method: 'POST',
                url: URL + '/api/v1/order',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $httpParamSerializerJQLike(request)
            }).success(orderSuccess).error(orderError);
        }

        function orderError(data){
          console.log('error!');
        }

        function orderSuccess(order){
            // Go to the succes page
            $state.go('app.success');

            // If app is still at the succes page after 2 sec, go to dashboard
            $timeout(function(){
                if($state.includes('app.success')) $state.go('app.home');
            }, 2000);
            
            // Empty the cart
            cart.items = [];
            cart.count = 0;

            // Reset products
            getData();

            orders.push(order);
            console.log(orders);
            // TODO: Add order to user orders in cookies
        }

        //getData();
        initApp();

        return {
            init : function(){
                return initApp();
            },
            // Set and get the id of the bar
            setId : function(id){
                bar_id = id;
            },

            getId : function(){
                return bar_id;
            },

            bar : function(){
                return bar;
            },

            // create the order
            order : function(note){
                if(cart.items.length > 0) order(note);
            },

            // get a product
            product : function(id){
                return products[id];
            },

            // get the cart
            cart : function(id){
                return cart;
            },

            sales : function() {
                return sales;
            },

            // get popular
            popular : function() {
                return popular;
            },

            // get categories
            categories : function() {
                return categories;
            },

            category: function(id) {
                return categories[id];
            },

            orders: function() {
                return orders;
            },

            updateOrders: function(){
                updateOrders();
            }
        }

    }

})();

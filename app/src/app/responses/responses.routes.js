;(function () {
    'use strict';

    angular.module('app.responses')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('error', {
                cache: false, // false will reload on every visit.
                controller: 'ResponsesController as vm',
                templateUrl: '/app/html/responses/oops.view.html',
                url: '/oops'
            })
            .state('app.success', {
                cache: false, // false will reload on every visit.
                controller: 'ResponsesController as vm',
                templateUrl: '/app/html/responses/success.view.html',
                url: '/success'
            });
    }

})();

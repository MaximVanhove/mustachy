;(function () {
    'use strict';

    angular.module('app.responses')
        .controller('ResponsesController', ResponsesController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    ResponsesController.$inject = [
        // Angular
        // Other
        'Shop'
    ];

    function ResponsesController(
        // Angular
        Shop
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Oops'
        }
    }

})();

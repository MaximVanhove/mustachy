;(function () {
    'use strict';

    angular.module('app.cart')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('app.cart', {
                cache: false, // false will reload on every visit.
                controller: 'CartController as vm',
                templateUrl: '/app/html/cart/cart.view.html',
                url: '/cart'
            });
    }

})();

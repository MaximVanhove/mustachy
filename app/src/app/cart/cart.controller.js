;(function () {
    'use strict';

    angular.module('app.cart')
        .controller('CartController', CartController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    CartController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http',
        // Other
        'Shop'
    ];

    function CartController(
        // Angular
        $log,
        $scope,
        $http,
        // Other
        Shop
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            title: 'Cart'
        }

        vm.cart = Shop.cart;
        vm.product = Shop.product;
        vm.checkout = { note: '' };

        vm.order = function(){
            Shop.order(vm.checkout.note);
        };
    }

})();

;(function () {
    'use strict';

    // Module declarations
    var app = angular.module('app')
        .controller('AppController', AppController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    AppController.$inject = [
        // Angular
        '$location',
        '$log',
        // Other
        '$state',
        'Shop'
    ];

    function AppController(
        // Angular
        $location,
        $log,
        // Other
        $state,
        Shop
    ) {
        // ViewModel
        // =========
        var app = this;


        var regex = new RegExp("\#\/start\/[0-9]*");
        var start = regex.test(window.location.hash);

        if(!start){
            var success = Shop.init();
            if(success == false){
                $location.path('oops').replace();
            }
        }



        // User Interface
        // --------------
        app.$$ui = {
            state : function(name){
                return $state.includes(name);
            }
        }

        app.cart = Shop.cart;
    }
})();

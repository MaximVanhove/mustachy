;(function () {
    'use strict';

    angular.module('app.home')
        .controller('HomeController', HomeController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    HomeController.$inject = [
        // Angular
        '$interval',
        '$log',
        '$scope',
        '$http',
        // Other
        'Shop',
        'URL'
    ];

    function HomeController(
        // Angular
        $interval,
        $log,
        $scope,
        $http,
        // Other
        Shop,
        URL
    ) {
        // ViewModel
        // =========
        var vm = this;

        vm.url = URL;

        vm.sales = Shop.sales;
        vm.popular = Shop.popular;
        vm.product = Shop.product;
        vm.orders = Shop.orders;
        vm.bar = Shop.bar;

        function refresh(){
            Shop.updateOrders();
        }

        var promise = $interval(refresh, 1000);

        // Cancel interval on page changes
        $scope.$on('$destroy', function(){
            if (angular.isDefined(promise)) {
                $interval.cancel(promise);
                promise = undefined;
            }
        });

    }

})();

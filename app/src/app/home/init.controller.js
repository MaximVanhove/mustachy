;(function () {
    'use strict';

    angular.module('app.home')
        .controller('InitController', InitController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    InitController.$inject = [
        '$cookies',
        '$state',
        'Shop'
    ];

    function InitController(
        $cookies,
        $state,
        Shop
    ) {
        // ViewModel
        // =========
        var vm = this;

        $cookies.putObject('bar_id', {id:  $state.params.init_id});
        Shop.setId($state.params.init_id);
        Shop.init();
        $state.go('app.home');
    }

})();

;(function () {
    'use strict';

    angular.module('app.home')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('app.home', {
                cache: false, // false will reload on every visit.
                controller: 'HomeController as vm',
                templateUrl: '/app/html/home/home.view.html',
                url: '/'
            })
            .state('init', {
                cache: false, // false will reload on every visit.
                controller: 'InitController as vm',
                url: '/start/{init_id:int}'
            });
    }

})();
